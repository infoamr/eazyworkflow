<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<div class="clearfix mtop20"></div>
<div class="">
  <div class="email-media">
      <div class="media mt-0">
        <?php //print_r($response); exit;?>        
        
        <div class="media-body">
          <div class="float-right d-md-flex fs-15">
            <small class="mr-2"><?php $ReceivedDateTime = date("Y-m-d H:i:s",mktime($response['ReceivedDateTime']));
            echo $ReceivedDateTime; ?></small>
            
          </div>
          <div class="media-title text-dark font-weight-semiblod"><?php echo $response['Sender']['EmailAddress']['Name'];?> <span class="text-muted">( <?php echo $response['Sender']['EmailAddress']['Address'];?> )</span></div>
         <?php 
            $toArr = array();
            $ccArr = array();

          foreach($response['ToRecipients'] as $to)
          {
            $toArr[] = $to['EmailAddress']['Name'].' ('.$to['EmailAddress']['Address'].')';  
          } 

          $toStr = implode(", ",$toArr);     
          echo '<p class="mb-0 font-weight-semiblod">To: '.$toStr.'</p>';

          foreach($response['CcRecipients'] as $cc)
          {
            $ccArr[] = $cc['EmailAddress']['Name'].' ('.$cc['EmailAddress']['Address'].')';
          }
          $ccStr = implode(", ",$ccArr);
          echo '<p class="mb-0 font-weight-semiblod">CC: '.$ccStr.'</p>';

          foreach($response['ReplyTo'] as $rp)
          {
            //echo '<p class="mb-0 font-weight-semiblod">CC: '.$cc['EmailAddress']['Name'].'(' .$cc['EmailAddress']['Address'].')</p>';
          }
          
          ?>
        </div>
      </div>
    </div>
    <div class="eamil-body">
        <p><?php echo $response['Body']['Content']?></p>
        <hr>
    </div>
    <br/>
    <div class="pull-right" style="margin-top: -25px;">      
        <a href="<?php echo admin_url().'mailbox/reply/'.$id.'';?>" autocomplete="off" data-loading-text="<?php echo _l('wait_text'); ?>" class="btn btn-warning">
        <i class="fa fa-reply"></i></i> <?php echo _l('mailbox_reply'); ?></a>

        <a href="<?php echo admin_url().'mailbox/reply/'.$id.'/forward';?>" autocomplete="off" data-loading-text="<?php echo _l('wait_text'); ?>" class="btn btn-info">
        <i class="fa fa-share"></i><?php echo _l('mailbox_forward'); ?></a>
    </div>
</div>
<script>
  var mailid = <?php echo $id;?>;
  
</script>