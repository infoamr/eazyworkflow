<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Authentication extends App_Controller
{
    public function __construct()
    {
        parent::__construct();

        if ($this->app->is_db_upgrade_required()) {
            redirect(admin_url());
        }

        load_admin_language();
        $this->load->model('Authentication_model');
        $this->load->library('form_validation');

        $this->form_validation->set_message('required', _l('form_validation_required'));
        $this->form_validation->set_message('valid_email', _l('form_validation_valid_email'));
        $this->form_validation->set_message('matches', _l('form_validation_matches'));

        hooks()->do_action('admin_auth_init');
    }

    public function index()
    {
        $this->admin();
    }

    public function admin()
    {
        if (is_staff_logged_in()) {
            redirect(admin_url());
        }

        $this->form_validation->set_rules('password', _l('admin_auth_login_password'), 'required');
        $this->form_validation->set_rules('email', _l('admin_auth_login_email'), 'trim|required|valid_email');
        if (show_recaptcha()) {
            $this->form_validation->set_rules('g-recaptcha-response', 'Captcha', 'callback_recaptcha');
        }
        if ($this->input->post()) {
            if ($this->form_validation->run() !== false) {
                $email    = $this->input->post('email');
                $password = $this->input->post('password', false);
                $remember = $this->input->post('remember');

                $data = $this->Authentication_model->login($email, $password, $remember, true);

                if (is_array($data) && isset($data['memberinactive'])) {
                    set_alert('danger', _l('admin_auth_inactive_account'));
                    redirect(admin_url('authentication'));
                } elseif (is_array($data) && isset($data['two_factor_auth'])) {
                    $this->Authentication_model->set_two_factor_auth_code($data['user']->staffid);

                    $sent = send_mail_template('staff_two_factor_auth_key', $data['user']);

                    if (!$sent) {
                        set_alert('danger', _l('two_factor_auth_failed_to_send_code'));
                        redirect(admin_url('authentication'));
                    } else {
                        set_alert('success', _l('two_factor_auth_code_sent_successfully', $email));
                    }
                    redirect(admin_url('authentication/two_factor'));
                } elseif ($data == false) {
                    set_alert('danger', _l('admin_auth_invalid_email_or_password'));
                    redirect(admin_url('authentication'));
                }

                $this->load->model('announcements_model');
                $this->announcements_model->set_announcements_as_read_except_last_one(get_staff_user_id(), true);

                // is logged in
                maybe_redirect_to_previous_url();

                hooks()->do_action('after_staff_login');
                redirect(admin_url());
            }
        }

        $data['title'] = _l('admin_auth_login_heading');
        $this->load->view('authentication/login_admin', $data);
    }

    public function two_factor()
    {
        $this->form_validation->set_rules('code', _l('two_factor_authentication_code'), 'required');

        if ($this->input->post()) {
            if ($this->form_validation->run() !== false) {
                $code = $this->input->post('code');
                $code = trim($code);
                if ($this->Authentication_model->is_two_factor_code_valid($code)) {
                    $user = $this->Authentication_model->get_user_by_two_factor_auth_code($code);
                    $this->Authentication_model->clear_two_factor_auth_code($user->staffid);
                    $this->Authentication_model->two_factor_auth_login($user);

                    $this->load->model('announcements_model');
                    $this->announcements_model->set_announcements_as_read_except_last_one(get_staff_user_id(), true);

                    maybe_redirect_to_previous_url();

                    hooks()->do_action('after_staff_login');
                    redirect(admin_url());
                } else {
                    set_alert('danger', _l('two_factor_code_not_valid'));
                    redirect(admin_url('authentication/two_factor'));
                }
            }
        }
        $this->load->view('authentication/set_two_factor_auth_code');
    }

    public function forgot_password()
    {
        if (is_staff_logged_in()) {
            redirect(admin_url());
        }
        $this->form_validation->set_rules('email', _l('admin_auth_login_email'), 'trim|required|valid_email|callback_email_exists');
        if ($this->input->post()) {
            if ($this->form_validation->run() !== false) {
                $success = $this->Authentication_model->forgot_password($this->input->post('email'), true);
                if (is_array($success) && isset($success['memberinactive'])) {
                    set_alert('danger', _l('inactive_account'));
                    redirect(admin_url('authentication/forgot_password'));
                } elseif ($success == true) {
                    set_alert('success', _l('check_email_for_resetting_password'));
                    redirect(admin_url('authentication'));
                } else {
                    set_alert('danger', _l('error_setting_new_password_key'));
                    redirect(admin_url('authentication/forgot_password'));
                }
            }
        }
        $this->load->view('authentication/forgot_password');
    }

    public function reset_password($staff, $userid, $new_pass_key)
    {
        if (!$this->Authentication_model->can_reset_password($staff, $userid, $new_pass_key)) {
            set_alert('danger', _l('password_reset_key_expired'));
            redirect(admin_url('authentication'));
        }
        $this->form_validation->set_rules('password', _l('admin_auth_reset_password'), 'required');
        $this->form_validation->set_rules('passwordr', _l('admin_auth_reset_password_repeat'), 'required|matches[password]');
        if ($this->input->post()) {
            if ($this->form_validation->run() !== false) {
                hooks()->do_action('before_user_reset_password', [
                    'staff'  => $staff,
                    'userid' => $userid,
                ]);
                $success = $this->Authentication_model->reset_password($staff, $userid, $new_pass_key, $this->input->post('passwordr', false));
                if (is_array($success) && $success['expired'] == true) {
                    set_alert('danger', _l('password_reset_key_expired'));
                } elseif ($success == true) {
                    hooks()->do_action('after_user_reset_password', [
                        'staff'  => $staff,
                        'userid' => $userid,
                    ]);
                    set_alert('success', _l('password_reset_message'));
                } else {
                    set_alert('danger', _l('password_reset_message_fail'));
                }
                redirect(admin_url('authentication'));
            }
        }
        $this->load->view('authentication/reset_password');
    }

    public function set_password($staff, $userid, $new_pass_key)
    {
        if (!$this->Authentication_model->can_set_password($staff, $userid, $new_pass_key)) {
            set_alert('danger', _l('password_reset_key_expired'));
            if ($staff == 1) {
                redirect(admin_url('authentication'));
            } else {
                redirect(site_url('authentication'));
            }
        }
        $this->form_validation->set_rules('password', _l('admin_auth_set_password'), 'required');
        $this->form_validation->set_rules('passwordr', _l('admin_auth_set_password_repeat'), 'required|matches[password]');
        if ($this->input->post()) {
            if ($this->form_validation->run() !== false) {
                $success = $this->Authentication_model->set_password($staff, $userid, $new_pass_key, $this->input->post('passwordr', false));
                if (is_array($success) && $success['expired'] == true) {
                    set_alert('danger', _l('password_reset_key_expired'));
                } elseif ($success == true) {
                    set_alert('success', _l('password_reset_message'));
                } else {
                    set_alert('danger', _l('password_reset_message_fail'));
                }
                if ($staff == 1) {
                    redirect(admin_url('authentication'));
                } else {
                    redirect(site_url());
                }
            }
        }
        $this->load->view('authentication/set_password');
    }

    public function logout()
    {
        $this->Authentication_model->logout();
        hooks()->do_action('after_user_logout');
        redirect(admin_url('authentication'));
    }

    public function email_exists($email)
    {
        $total_rows = total_rows(db_prefix().'staff', [
            'email' => $email,
        ]);
        if ($total_rows == 0) {
            $this->form_validation->set_message('email_exists', _l('auth_reset_pass_email_not_found'));

            return false;
        }

        return true;
    }

    public function recaptcha($str = '')
    {
        return do_recaptcha_validation($str);
    }
    
    public function twostep_verify()
    {
        if($this->input->post())
        {
            $this->form_validation->set_rules('authtype','Type','required',array('required' => 'Please select an option to get code'));
            if($this->form_validation->run() == TRUE)
            {
                $data = $this->input->post();
                if($data['authtype'] == 1) {
                    $this->session->set_userdata('stf_phn', $data['phonenum']);
                    redirect(admin_url('authentication/sendotp'));
                }
                else {
                    $this->db->where('staffid', $data['stid']);
                    $user = $this->db->get(db_prefix() . 'staff')->row();

                    $this->Authentication_model->set_two_factor_auth_code($data['stid']);
                    $sent = send_mail_template('staff_two_factor_auth_key', $user);

                    if (!$sent) {
                        set_alert('danger', _l('two_factor_auth_failed_to_send_code'));
                        redirect(admin_url('authentication'));
                    } else {
                        set_alert('success', _l('two_factor_auth_code_sent_successfully'));
                    }
                        redirect(admin_url('authentication/two_factor'));
                }
            }
        }

        $data['staffid'] = $_SESSION['stf_id'];
        $this->load->view('authentication/staff_auth_method', $data);
    }

    public function sendotp()
    {
        $this->db->where('phonenumber', $_SESSION['stf_phn']);
        $user = $this->db->get(db_prefix() . 'staff')->row();
        if(!empty($user)){
            $fullname = $user->firstname.' '.$user->lastname;
        }
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => D7API_URL."/send",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => array('mobile' => $user->phonenumber,'sender_id' => 'RamAsso','message' => 'Hi '.$fullname.', Your otp code for Ram Associates Workflow login is {code}','expiry' => '300'),
          CURLOPT_HTTPHEADER => array(
            "Authorization: Token ".D7API_TOKEN
          ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);
        //echo $response;
        $result = json_decode($response);
        //print_r($result); exit;
        // stdClass Object ( [otp_id] => 2a82698c-2c7b-4213-bbdf-b55d4eef836a [status] => open [expiry] => 300 )

        if ($result) {
            $data['otp_id'] = $result->otp_id;
            $this->load->view('authentication/verify_otp', $data);
        }

        return false;
    }

    public function resendotp($otp_id)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => D7API_URL."/resend",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => array('otp_id' => $otp_id),
          CURLOPT_HTTPHEADER => array(
            "Authorization: Token ".D7API_TOKEN
          ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);
        // echo $response;
        $result = json_decode($response);

        if ($result) {
            $this->load->view('authentication/verify_otp', $data);
        }

        return false;
    }

    public function verifyotp()
    {
        if($this->input->post())
        {
            $this->form_validation->set_rules('code','Code','required');
            if($this->form_validation->run() == TRUE)
            {
                $post = $this->input->post();
                $curl = curl_init();

                curl_setopt_array($curl, array(
                  CURLOPT_URL => D7API_URL."/verify",
                  CURLOPT_RETURNTRANSFER => true,
                  CURLOPT_ENCODING => "",
                  CURLOPT_MAXREDIRS => 10,
                  CURLOPT_TIMEOUT => 0,
                  CURLOPT_FOLLOWLOCATION => true,
                  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                  CURLOPT_CUSTOMREQUEST => "POST",
                  CURLOPT_POSTFIELDS => array('otp_id' => $post['otp_id'],'otp_code' => $post['code']),
                  CURLOPT_HTTPHEADER => array(
                    "Authorization: Token ".D7API_TOKEN
                  ),
                ));

                $response = curl_exec($curl);
                curl_close($curl);
                $result = json_decode($response);

                // print_r($result); exit;
                // stdClass Object ( [status] => success )
                // stdClass Object ( [otp_id] => Invalid otp id )
                // stdClass Object ( [status] => failed [error] => Otp code expired. )

                if ($result->status == "success") {
                    $data = $this->Authentication_model->loginuser($_SESSION['stf_email'],
                        $_SESSION['stf_pwd'], $_SESSION['stf_rem'], true);
                    redirect(admin_url('authentication'));
                }
                if ($result->status == "failed") {
                    $data['error'] = 'Otp code expired';
                    $this->load->view('authentication/verify_otp', $data);
                }
                if (isset($result->status)) {
                }
                else {
                    $data['error'] = 'Invalid otp';
                    $this->load->view('authentication/verify_otp', $data);
                }
            }
        }
    }
}
