<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
   <div class="content">
      <div class="row">
        <?php echo form_open(admin_url('payroll/pay')); ?>
          <div class="col-md-12">
            <div class="panel_s">
              <div class="panel-body">
                <h4 class="customer-profile-group-heading">Preview Payroll</h4>
                <div class="row">
                  <div class="col-md-12" style="background-color: #f9fafc; padding-top: 20px; padding-bottom: 10px">
                    <div class="col-md-6">
                      <div class="form-group col-md-12" style="margin: 0px; padding: 0px">
                        <h5 class="col-md-4 mt-0" style="font-weight: bold; font-size: 13px; color:#03A9F4;">Company</h5>
                        <p class="col-md-8 mt-0" style="font-weight: normal; font-size: 13px"><?php echo get_company_name($this->session->userdata('client')); ?></p>
                        <input type="hidden" name="client" value="<?php echo $this->session->userdata('client'); ?>">
                      </div>
                      <div class="form-group col-md-12" style="margin: 0px; padding: 0px"> 
                        <h5 class="col-md-4 mt-0" style="font-weight: bold; font-size: 13px; color:#03A9F4;">Federal Form</h5> 
                        <p class="col-md-8 mt-0" style="font-weight: normal; font-size: 13px"><?php echo get_federal_fullname($this->session->userdata('federal_form')); ?></p>
                        <input type="hidden" name="federal_form" value="<?php echo $this->session->userdata('federal_form'); ?>">
                      </div>
                      <div class="form-group col-md-12" style="margin: 0px; padding: 0px"> 
                        <h5 class="col-md-4 mt-0" style="font-weight: bold; font-size: 13px; color:#03A9F4;">Form Status</h5> 
                        <?php 
                          if($this->session->userdata('federal_status') == 1) {
                            $fedstatus = 'Pending';
                          }
                          if($this->session->userdata('federal_status') == 2) {
                            $fedstatus = 'In Progress';
                          }
                          if($this->session->userdata('federal_status') == 3) {
                            $fedstatus = 'Filed';
                          } ?>
                          <p class="col-md-8 mt-0" style="font-weight: normal; font-size: 13px">
                          <?php echo $fedstatus; ?></p>
                          <input type="hidden" name="federal_status" value="<?php echo $this->session->userdata('federal_status'); ?>">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group col-md-12" style="margin: 0px; padding: 0px">
                        <h5 class="col-md-4 mt-0" style="font-weight: bold; color:#03A9F4;">Priority</h5>
                        <?php 
                        if($this->session->userdata('priority') == 1) {
                          $priority = 'Low';
                        }
                        if($this->session->userdata('priority') == 2) {
                          $priority = 'Medium';
                        }
                        if($this->session->userdata('priority') == 3) {
                          $priority = 'High';
                        }
                        if($this->session->userdata('priority') == 4) {
                          $priority = 'Urgent';
                        } ?>
                        <p class="col-md-8 mt-0" style="font-weight: normal; font-size: 13px"><?php echo $priority; ?></p>
                        <input type="hidden" name="priority" value="<?php echo $this->session->userdata('priority'); ?>">
                      </div>
                      <div class="form-group col-md-12" style="margin: 0px; padding: 0px">
                        <h5 class="col-md-4 mt-0" style="font-weight: bold; color:#03A9F4;">Assigned To</h5>
                       <p class="col-md-8 mt-0" style="font-weight: normal; font-size: 13px"><?php echo get_staff_full_name($this->session->userdata('assigned_to')); ?></p>
                       <input type="hidden" name="assigned_to" value="<?php echo $this->session->userdata('assigned_to'); ?>">
                      </div>
                      <div class="form-group col-md-12" style="margin: 0px; padding: 0px">
                        <h5 class="col-md-4 mt-0" style="font-weight: bold; color:#03A9F4;">Start Date</h5>
                        <p class="col-md-8 mt-0" style="font-weight: normal; font-size: 13px"><?php $stdate = date("d F Y", strtotime($this->session->userdata('startdate'))); echo $stdate; ?></p>
                        <input type="hidden" name="startdate" value="<?php echo $this->session->userdata('startdate'); ?>">
                      </div>
                      <div class="form-group col-md-12" style="margin: 0px; padding: 0px">
                        <h5 class="col-md-4 mt-0" style="font-weight: bold; color:#03A9F4;">Due Date</h5>
                        <p class="col-md-8 mt-0" style="font-weight: normal; font-size: 13px"><?php $ddate = date("d F Y", strtotime($this->session->userdata('duedate'))); echo $ddate; ?></p>
                        <input type="hidden" name="duedate" value="<?php echo $this->session->userdata('duedate'); ?>">
                      </div>
                    </div>
                  </div>

                  <!-- Table Starts-->
                  <div class="col-md-12" >
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th class="text-center"><h5 style="font-weight: bold; color:#03A9F4;">State</h5></th>
                            <th class="text-center"><h5 style="font-weight: bold; color:#03A9F4;">Form</h5></th>
                            <th class="text-center"><h5 style="font-weight: bold; color:#03A9F4;">Status</h5></th>
                          </tr>
                        </thead>
                        <tbody id="tbody">
                          <?php foreach($this->session->userdata('stateform') as $stf) { 
                            $CI = &get_instance();
                            $stateform = $CI->db->select('*')->where('id', $stf['id'])->get(db_prefix() . 'state_forms')->result();?>
                          <tr>
                            <td class="text-center"><?php echo $stateform[0]->state;?></td>
                            <td class="text-center"><?php echo $stateform[0]->form;?></td>
                            <td class="text-center"><?php echo $stateform[0]->state_type;?></td>
                          </tr>
                          <?php }?>
                        </tbody>
                      </table>
                  </div>
                  <!-- Table Ends -->

                  
                  <div class="col-md-12" style="background-color: #f9fafc; margin-top: 5px; padding-top: 20px; padding-bottom: 10px">
                    <div class="col-md-4" >
                       <h5 class="mbot15" style="font-weight: bold; color:#03A9F4;"><?php echo _l('reminders').' ('._l('set_reminder_date').' )'; ?></h5>
                       <p style="font-weight: normal; font-size: 13px"><?php $rmd = date("d F Y", strtotime($this->session->userdata('reminder_date'))); echo $rmd;?></p>
                       <input type="hidden" name="reminder_date" value="<?php echo $this->session->userdata('reminder_date'); ?>">
                    </div>

                    <div class="col-md-4" >
                       <h5 class="mbot15" style="font-weight: bold; color:#03A9F4;">Reminder to</h5>
                       <p style="font-weight: normal; font-size: 13px"><?php echo $this->session->userdata('staffemail'); ?></p>
                       <input type="hidden" name="staffemail" value="<?php echo $this->session->userdata('staffemail'); ?>">
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                          <h5 class="mbot15" style="font-weight: bold; color:#03A9F4;">Description</h5>
                          <p style="font-weight: normal; font-size: 13px"><?php echo $this->session->userdata('reminder_desc'); ?>
                          <input type="hidden" name="reminder_description" value="<?php echo $this->session->userdata('reminder_desc'); ?>">
                          </p>
                        </div>
                    </div>
                  </div>
                 

                  <?php if ($fetch_data[0]['recurring'] == 1) { ?>
                  <div class="col-md-12" style="background-color: #f9fafc; margin-top: 5px; padding-top: 20px; padding-bottom: 10px">
                    <div class="col-md-3">
                      <div class="form-group">
                        <h5 class="mbot15" style="font-weight: bold; color:#03A9F4;">Recurring Type</h5>
                        <p style="font-weight: normal; font-size: 13px"><?php echo $fetch_data[0]['recurring_type'] ?></p>
                      </div>
                    </div>

                    <div class="col-md-3">  
                      <div class="form-group">
                        <h5 class="mbot15" style="font-weight: bold; color:#03A9F4;">Count</h5>
                        <p style="font-weight: normal; font-size: 13px"><?php echo $fetch_data[0]['repeat_every'] ?></p>
                      </div>
                    </div>
                  
                    <div class="col-md-3"> 
                      <div class="form-group">
                        <h5 class="mbot15" style="font-weight: bold; color:#03A9F4;">Every</h5>
                        <p style="font-weight: normal; font-size: 13px"><?php echo $fetch_data[0]['repeat_every'] ?></p>
                      </div>
                    </div>

                    <div class="col-md-3">
                      <div class="form-group">
                        <h5 class="mbot15" style="font-weight: bold; color:#03A9F4;">Cycles</h5>
                        <p style="font-weight: normal; font-size: 13px"><?php echo $fetch_data[0]['cycles'] ?></p>
                      </div>
                    </div>
                  </div>
                  <?php } ?>

                  <div class="col-md-12" style="background-color: #f9fafc; margin-top: 5px; padding-top: 20px; padding-bottom: 10px">
                      <div class="col-md-6">
                        <div class="form-group">
                          <div class="form-group">
                            <h5 class="mbot15" style="font-weight: bold; color:#03A9F4;">Followers</h5>
                            <?php if(!empty($this->session->userdata('followers'))) { ?>
                              <input type="hidden" name="followers" value="<?php print_r($this->session->userdata('followers')); ?>">
                              <ul class="nav navbar-nav">
                              <?php foreach($this->session->userdata('followers') as $fl) { ?>
                              <li class="list-group-item" style="width: 240px"><?php echo get_staff_full_name($fl); ?></li>
                               <?php } ?>
                              </ul>
                            <?php } 
                            else { ?>
                              <p style="font-weight: normal; font-size: 13px">No Followers</p>
                            <?php } ?>
                          </div>
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group">
                          <div class="form-group">
                            <h5 class="mbot15" style="font-weight: bold; color:#03A9F4;">Remarks</h5>
                            <p style="font-weight: normal; font-size: 13px"><?php echo $this->session->userdata('remarks'); ?></p>
                            <input type="hidden" name="remarks" value="<?php echo $this->session->userdata('remarks'); ?>">
                          </div>
                        </div>
                      </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="btn-bottom-toolbar btn-toolbar-container-out text-right custom_width">
            <a class="btn btn-primary" href="<?php echo admin_url('payroll/pay'); ?>">Change</a>
            <button class="btn btn-info only-save customer-form-submiter">Continue to Save
            </button>
          </div>
          <?php echo form_close(); ?>
      </div>
   </div>
</div>
<?php init_tail(); ?>
</body>
</html>
