<?php defined('BASEPATH') or exit('No direct script access allowed');
   $totalQuickActionsRemoved = 0;
   $quickActions = $this->app->get_quick_actions_links();
   foreach($quickActions as $key => $item){
    if(isset($item['permission'])){
     if(!has_permission($item['permission'],'','create')){
       $totalQuickActionsRemoved++;
     }
   }
   }
   ?>
<nav class="navbar navbar-default header" style="margin-bottom: 0px"> 
  <div class="container">
    <ul class="nav navbar-nav" >
      <?php foreach($sidebar_menu as $key => $item){
         if(isset($item['collapse']) && count($item['children']) === 0) {
           continue;
         }
         ?>
      <li class="drd" class="dropdown customers-nav-item-<?php echo $item['slug']; ?> " 
         <?php echo _attributes_to_string(isset($item['li_attributes']) ? $item['li_attributes'] : []); ?>>
         <a href="<?php echo count($item['children']) > 0 ? '#' : $item['href']; ?>" 
          aria-expanded="false"  
          <?php echo _attributes_to_string(isset($item['href_attributes']) ? $item['href_attributes'] : []); ?>>
             <i class="<?php echo $item['icon']; ?> typcn typcn-book" style="color:#000; font-size: 18px; margin-right: 5px;"></i>
             <span class="menu-text" style="color:#000">
             <?php echo '    '._l($item['name'],'', false); ?>
             </span>
             <?php if(count($item['children']) > 0){ ?>
             <i class="caret" style="color:#000"></i>
             <?php } ?>
         </a>
         
         <?php if(count($item['children']) > 0){ ?>
         <ul class="dropdown submenu-item" aria-expanded="true">
            <?php foreach($item['children'] as $submenu){
               ?>
            <li class="customers-nav-item-<?php echo $submenu['slug']; ?>"
              <?php echo _attributes_to_string(isset($submenu['li_attributes']) ? $submenu['li_attributes'] : []); ?>>
              <a aria-haspopup="true" href="<?php echo $submenu['href']; ?>" class="nav-link"
               <?php echo _attributes_to_string(isset($submenu['href_attributes']) ? $submenu['href_attributes'] : []); ?>>
               <?php if(!empty($submenu['icon'])){ ?>
               <i class="<?php echo $submenu['icon']; ?> typcn-book"></i>
               <?php } ?>
               <span class="">
                  <?php echo _l($submenu['name'],'',false); ?>
               </span>
               </a>
            </li>
            <?php } ?>
         </ul>
         <?php } ?>
       
      </li>
      <?php hooks()->do_action('after_render_single_aside_menu', $item); ?>
      <?php } ?>
      <?php if($this->app->show_setup_menu() == true && (is_staff_member() || is_admin())){ ?>
      <li<?php if(get_option('show_setup_menu_item_only_on_hover') == 1) { echo ' style="display:none;"'; } ?> id="setup-menu-item" class="customers-nav-item">
         <a href="#" class="open-customizer"><i class="fa fa-cog typcn-book" style="color: #000000"></i>
         <span class="menu-text" style="font-size: 15px; font-weight: 600; color: #000000">
            <?php echo _l('setting_bar_heading'); ?>
            <?php
                if ($modulesNeedsUpgrade = $this->app_modules->number_of_modules_that_require_database_upgrade()) {
                  echo '<span class="badge menu-badge bg-warning">' . $modulesNeedsUpgrade . '</span>';
                }
            ?>
         </span>
         </a>
         <?php } ?>
      </li>
      <?php hooks()->do_action('after_render_aside_menu'); ?>
      <?php $this->load->view('admin/projects/pinned'); ?>
   </ul>

</div>
</nav> 



<!-- <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
  
  <div class="collapse navbar-collapse" id="main_nav">
<ul class="navbar-nav">
  <li class="nav-item active"> <a class="nav-link" href="#">Home </a> </li>
  <li class="nav-item"><a class="nav-link" href="#"> About </a></li>
  <li class="nav-item"><a class="nav-link" href="#"> Services </a></li>
  <li class="nav-item dropdown">
     <a class="nav-link  dropdown-toggle" href="#" data-toggle="dropdown">  More items  </a>
      <ul class="dropdown-menu">
        <li><a class="dropdown-item" href="#"> Submenu item 1</a></li>
        <li><a class="dropdown-item" href="#"> Submenu item 2 </a></li>
      </ul>
  </li>
</ul>
  </div> 
</nav> -->
