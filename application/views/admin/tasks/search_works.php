<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
   <div class="content">
      <div class="row">
         <div class="col-md-12">
            <div class="panel_s">
               <div class="panel-body">
                  <div class="clearfix"></div>
                  <div class="row">
                    <div class="col-md-12">
                    <?php echo form_open(admin_url('tasks/search_worksreport')); ?>
                     <div class="col-md-2">
                        <?php echo render_date_input('from','','From'); ?>
                     </div>
                     <div class="col-md-2">
                        <?php echo render_date_input('to','','To'); ?>
                     </div>

                     <!-- <div class="col-md-2">
                        <div class="select-placeholder">
                           <select name="assigned_to" id="assigned_to" class="selectpicker" data-live-search="true" data-width="100%">
                              <option value=""><?php //echo _l('all_staff_members'); ?></option>
                              <?php //foreach($staff as $st){ ?>
                              <option value="<?php //echo $st['staffid']; ?>"><?php //echo $st['full_name']; ?></option>
                              <?php //} ?>
                           </select>
                        </div>
                     </div> -->

                      <div class="col-md-3">
                        <div class="select-placeholder">
                        <!-- <label for="rel_type" class="control-label">Work Type</label> -->
                          <select name="work_type" class="selectpicker" id="work_type" data-live-search="true" data-width="100%" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>">
                             <option value="">All Work Type</option>
                             <?php foreach($work_type_data->result() as $work_type) { ?>
                             <option value="<?php echo $work_type->work_type?>" <?php if(isset($task) && $task->work_type == $work_type->work_type){echo 'selected';} ?>><?php echo $work_type->work_type?></option>
                             <?php } ?>
                          </select>
                        </div>
                      </div>

                     <div class="col-md-3">
                        <div class="select-placeholder">
                           <select id="statuses" name="statuses" data-live-search="true" data-width="100%" class="selectpicker">
                            <option value="">All Statuses</option>
                            <option value="1"><?php echo _l('task_status_1'); ?></option>
                            <option value="2"><?php echo _l('task_status_2'); ?></option>
                            <option value="3"><?php echo _l('task_status_3'); ?></option>
                            <option value="4"><?php echo _l('task_status_4'); ?></option>
                            <option value="5"><?php echo _l('task_status_5'); ?></option>
                            <option value="6"><?php echo _l('task_status_6'); ?></option>
                            <option value="7"><?php echo _l('task_status_7'); ?></option>
                            <option value="8"><?php echo _l('task_status_8'); ?></option>
                            <option value="9"><?php echo _l('task_status_9'); ?></option>
                            <option value="10"><?php echo _l('task_status_10'); ?></option>
                            <option value="11"><?php echo _l('task_status_11'); ?></option>
                            <option value="12"><?php echo _l('task_status_12'); ?></option>
                            <option value="13"><?php echo _l('task_status_13'); ?></option>
                            <option value="14"><?php echo _l('task_status_14'); ?></option>
                            <option value="15"><?php echo _l('task_status_15'); ?></option>
                           </select>
                        </div>
                     </div>
                     
                     <div class="col-md-2">
                      <button type="submit" class="btn btn-info pull-left"><?php echo _l('apply'); ?></button>
                     </div>
                    <?php echo form_close(); ?>
                    </div>

                    <div class="col-md-12">
                      <hr class="no-mtop"/>
                    </div>

                  </div>
                  <div class="clearfix"></div>
                  <table class="table dt-table scroll-responsive" data-order-col="6" data-order-type="desc">
                     <thead>
                     <tr>
                        <th>#Number</th>
                        <th>Work Title</th>
                        <th>Work Type</th>
                        <th>Client Name</th>
                        <th>Status</th> 
                        <th>Due Date</th>
                        <th>Created Date</th>
                        <th>Assigned to</th> 
                        <th>Priority</th>
                     </tr>
                     </thead>
                     <tbody>
                      <?php foreach($search_data as $sd) { ?>
                          <tr>
                            <?php $outputName = '';
                            $outputName = '<a href="'.admin_url('tasks/view/'.$sd['id']).'" onclick="init_task_modal('.$sd['id'].'); return false;">'.$sd['task_number'].'</a>';
                            if ($sd['recurring'] == 1) {
                              $outputName .= '<br /><span class="label label-primary inline-block mtop4">'. _l('recurring_task').'</span>';
                            } ?>
                            <td><?php echo $outputName; ?></td>
                            <td><?php echo $sd['name']; ?></td>
                            <td><?php echo $sd['work_type']; ?></td>
                            <td><?php echo $sd['company']; ?></td>
                            <?php if ($sd['status'] == 1) {
                                $status = _l('task_status_1');
                              } else if ($sd['status'] == 2) {
                                $status = _l('task_status_2');
                              } else if ($sd['status'] == 3) {
                                $status = _l('task_status_3');
                              } else if ($sd['status'] == 4) {
                                $status = _l('task_status_4');
                              } else if ($sd['status'] == 5) {
                                $status = _l('task_status_5');

                              } else if ($sd['status'] == 6) {
                                $status = _l('task_status_6');
                              } else if ($sd['status'] == 7) {
                                $status = _l('task_status_7');
                              } else if ($sd['status'] == 8) {
                                $status = _l('task_status_8');
                              } else if ($sd['status'] == 9) {
                                $status = _l('task_status_9');
                              } else if ($sd['status'] == 10) {
                                $status = _l('task_status_10');
                              } else if ($sd['status'] == 11) {
                                $status = _l('task_status_11');
                              } else if ($sd['status'] == 12) {
                                $status = _l('task_status_12');
                              } else if ($sd['status'] == 13) {
                                $status = _l('task_status_13');
                              } else if ($sd['status'] == 14) {
                                $status = _l('task_status_14');
                              } else {
                                $status = '';
                              }
                            ?>
                            <td><?php echo $status; ?></td>
                            <td><?php echo _d($sd['duedate']); ?></td>
                            <td><?php echo _d($sd['dateadded']); ?></td>
                            <?php $CI = &get_instance();
                              $assignees_ids = $CI->db->select('task_assigned.staffid, staff.firstname,staff.lastname')
                              // select('GROUP_CONCAT(staffid SEPARATOR ",") as assignees, staff.firstname,staff.lastname')
                              ->where('taskid', $sd['id'])
                              ->from(db_prefix() . 'task_assigned')
                              ->join(db_prefix() . 'staff', 'staff.staffid = task_assigned.staffid')
                              ->get()->result_array(); 

                              $names = array();
                              foreach ($assignees_ids as $aid) {
                                $names[] = $aid['firstname'].' '.$aid['lastname'];
                              }
                              $assignees = implode(", ", $names);
                            ?>
                            <td><?php echo $assignees; ?></td>
                            <?php if ($sd['priority'] == 1) {
                                $priority = _l('task_priority_low');
                              } else if ($sd['priority'] == 2) {
                                $priority = _l('task_priority_medium');
                              } else if ($sd['priority'] == 3) {
                                $priority = _l('task_priority_high');
                              } else if ($sd['priority'] == 4) {
                                $priority = _l('task_priority_urgent');
                              } else {
                                $priority = '';
                              } ?>
                            <td><?php echo $priority; ?></td>
                          </tr>
                      <?php } ?>
                     </tbody>
                     </table>
               </div>
         </div>
      </div>
   </div>
</div>
</div>
<?php init_tail(); ?>

<script>
  

</script>

</body>
</html>
