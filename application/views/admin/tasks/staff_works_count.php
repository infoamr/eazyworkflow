<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
  <div class="content">
    <div class="row">
      <div class="col-md-4">
        <div class="panel_s">
          <div class="panel-body">
            <div style="font-weight: bold; text-align: center;">
              <?php 
                $CI = &get_instance();
                $CI->db->select('count(id) as num');
                $CI->db->like('dateadded', date('Y-m-d'));
                $CI->db->from('tbltasks');
                $todcnt = $CI->db->get()->result();
                echo 'Total Works on '. date('d F Y').' is '.$todcnt[0]->num;
              ?>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-12">
              <hr class="no-mtop"/>
            </div>

            <table class="table table-striped dt-table scroll-responsive" data-order-col="1" data-order-type="desc">
              <thead class="thead-dark">
                <tr>
                  <th>Assigned Staff</th> 
                  <th>Today Works Count</th>
                </tr>
              </thead>
              <tbody>
                <?php 
                  foreach($staff as $st) { 
                    $CI = &get_instance();
                    $CI->db->select('count(tbltask_assigned.id) as number, tbltask_assigned.staffid, tbltask_assigned.taskid');
                    $CI->db->like('tbltasks.dateadded', date('Y-m-d'));
                    $CI->db->where('tbltask_assigned.staffid', $st['staffid']);
                    $CI->db->from('tbltask_assigned');
                    $CI->db->join('tbltasks', 'tbltasks.id = tbltask_assigned.taskid');
                    $todaytask = $CI->db->get()->result();
                    echo '<tr><td>'.$st['full_name'].'</td><td>'.$todaytask[0]->number.'</td></tr>';
                  } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="col-md-8">
        <div class="panel_s">
          <div class="panel-body">
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12">
               <?php echo form_open(admin_url('tasks/staff_works_count')); ?>
              <div class="col-md-3">
                  <?php echo render_date_input('from','','From'); ?>
              </div>

              <div class="col-md-3">
                <?php echo render_date_input('to','','To'); ?>
              </div>
               
              <div class="col-md-3">
                <div class="select-placeholder">
                   <select name="assigned_to" id="assigned_to" class="selectpicker" data-live-search="true" data-width="100%">
                      <option value=""><?php echo _l('all_staff_members'); ?></option>
                      <?php foreach($staff as $st){ ?>
                      <option value="<?php echo $st['staffid']; ?>"><?php echo $st['full_name']; ?></option>
                      <?php } ?>
                   </select>
                </div>
              </div>

               <div class="col-md-3">
                <button type="submit" class="btn btn-info"><?php echo _l('apply'); ?></button>
               </div>
              <?php echo form_close(); ?>
              </div>

              <div class="col-md-12">
                <hr class="no-mtop"/>
              </div>
            </div>

            <div class="clearfix"></div>
            <table class="table table-striped dt-table scroll-responsive" data-order-col="1" data-order-type="desc">
              <thead class="thead-dark">
              <tr>
               <th>Assigned Staff</th> 
               <th>Total Works Count</th>
              </tr>
              </thead>
              <tbody>
              <?php foreach($staff as $st) { 
                $CI = &get_instance();
                $CI->db->select('count(id) as number')->where('staffid', $st['staffid']);
                $count = $CI->db->get(db_prefix() . 'task_assigned')->result();
                echo '<tr><td>'.$st['full_name'].'</td><td>'.$count[0]->number.'</td></tr>';
              } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php init_tail(); ?>
</body>
</html>
