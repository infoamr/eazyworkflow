<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php if($task->settings->upload_files == 1){ ?>
  <?php echo form_open_multipart(site_url('clients/task/'.$task->id),array('class'=>'dropzone mbot15','id'=>'project-files-upload')); ?>
  <input type="file" name="file" multiple class="hide"/>
  <?php echo form_close(); ?>
  <div class="pull-left mbot20">
    <a href="<?php echo site_url('clients/download_all_project_files/'.$project->id); ?>" class="btn btn-info">
      <?php echo _l('download_all'); ?>
    </a>
  </div>
  <div class="pull-right mbot20">
   <button class="gpicker" data-on-pick="projectFileGoogleDriveSave">
    <i class="fa fa-google" aria-hidden="true"></i>
    <?php echo _l('choose_from_google_drive'); ?>
  </button>
  <div id="dropbox-chooser-project-files"></div>
</div>
<?php } ?>
<table class="table dt-table" data-order-col="4" data-order-type="desc">
  <thead>
    <tr style="font-weight: 600; font-size: 14px;">
      <th><?php echo _l('project_file_filename'); ?></th>
      <th><?php echo _l('project_file__filetype'); ?></th>
      <th>Uploaded By</th>
      <th><?php echo _l('project_file_dateadded'); ?></th>
      <?php if(get_option('allow_contact_to_delete_files') == 1){ ?>
        <th><?php echo _l('options'); ?></th>
      <?php } ?>
    </tr>
  </thead>
  <tbody>
    <?php foreach($files as $file){ ?>
      <tr>
       <td data-order="<?php echo $file['file_name']; ?>">
        <a href="<?php echo site_url('download/file/taskattachment/'. $file['attachment_key']); ?>" >
        <?php echo $file['file_name']; ?></a>
      </td>
      <?php $CI = &get_instance();
            $CI->db->select('firstname')->where('staffid', $file['staffid']);
            $stname = $CI->db->get(db_prefix() . 'staff')->result();
      ?>
      <td data-order="<?php echo $file['filetype']; ?>"><?php echo $file['filetype']; ?></td>
      <td data-order="<?php echo $file['file_name']; ?>"><?php echo $stname[0]->firstname; ?></td>
      <td data-order="<?php echo $file['dateadded']; ?>">
       <?php echo _dt($file['dateadded']); ?>
     </td>
     <?php if(get_option('allow_contact_to_delete_files') == 1) { ?>
       <td>
        <?php if($file['contact_id'] == get_contact_user_id()){ ?>
          <a href="<?php echo site_url('clients/delete_file/'.$file['id'].'/project'); ?>" class="btn btn-danger btn-icon _delete"><i class="fa fa-remove"></i></a>
        <?php } ?>
      </td>
    <?php } ?>
  </tr>
<?php } ?>
</tbody>
</table>
<div id="project_file_data"></div>
